from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Persona, Perro

import datetime

#importar user
from django.contrib.auth.models import User
#sistema de autenticación 
from django.contrib.auth import authenticate,logout, login as auth_login

from django.contrib.auth.decorators import login_required

# Create your views here.

def index(request):
    return render(request,'index.html')

def registro(request):
    return render(request,'formulario.html',{})


def crearPersona(request):
    correo = request.POST.get('correo','')
    run = request.POST.get('run','')
    nombrePersona = request.POST.get('nombrePersona','')
    fnacimiento = request.POST.get('fnacimiento','')
    telefono = request.POST.get('telefono',0)
    region = request.POST.get('region','')
    comuna = request.POST.get('comuna','')
    vivienda = request.POST.get('vivienda','')
    contrasenia = request.POST.get('contrasenia','')
    persona = Persona(correo=correo, run=run, nombrePersona=nombrePersona, fnacimiento=fnacimiento, telefono=telefono, region=region, comuna=comuna, vivienda=vivienda,contrasenia=contrasenia)
    persona.save()
    return redirect("index")

@login_required(login_url='login')
def buscarPersona(request,id):
    persona = Persona.objects.get(pk=id)
    return HttpResponse("correo : " +persona.correo+" run : "+persona.run+" nombre : " +persona.nombrePersona+" fnacimiento : "+persona.fnacimiento+" telefono : "+persona.telefono+" region : "+persona.region+" comuna : "+persona.comuna+" vivienda : "+persona.vivienda+" contrasenia : "+persona.contrasenia)

@login_required(login_url='login')
def editarPersona(request,id):
    persona = Persona.objects.get(pk=id)
    return render(request,'editarPersona.html',{'persona': persona})

@login_required(login_url='login')
def editadoPersona(request,id):
    persona = Persona.objects.get(pk=id)
    correo = request.POST.get('correo','')
    run = request.POST.get('run','')
    nombrePersona = request.POST.get('nombrePersona','')
    fnacimiento = request.POST.get('fnacimiento','')
    telefono = request.POST.get('telefono','')
    region = request.POST.get('region','')
    comuna = request.POST.get('comuna','')
    vivienda = request.POST.get('vivienda','')
    contrasenia = request.POST.get('contrasenia','')
    persona.correo = correo
    persona.run = run
    persona.nombrePersona = nombrePersona
    persona.fnacimiento = fnacimiento
    persona.telefono = telefono
    persona.region = region
    persona.comuna = comuna
    persona.vivienda = vivienda
    persona.contrasenia = contrasenia
    persona.save()
    return redirect('index')

@login_required(login_url='login')
def eliminarPersona(request,id):
    persona = Persona.objects.get(pk=id)
    persona.delete()
    return HttpResponse("Persona Eliminada")

@login_required(login_url='login')
def cerrar_session(request):
    del request.session['usuario']
    logout(request)
    return redirect('index')

def login(request):
    return render(request,'login.html',{})

def login_iniciar(request):
    usuario = request.POST.get('nombre_usuario','')
    contrasenia = request.POST.get('contrasenia','')
    user = authenticate(request,username=usuario, password=contrasenia)

    if user is not None:
        auth_login(request, user)
        request.session['usuario'] = user.first_name+" "+user.last_name
        return redirect("index")
    else:
        return redirect("login")

@login_required(login_url='login')
def crearPerro(request):
    foto = request.FILES.get('foto',False)
    nombrePerro = request.POST.get('nombrePerro','')
    raza = request.POST.get('raza','')
    descripcion = request.POST.get('descripcion','')
    estado = request.POST.get('estado','')
    perro = Perro(foto=foto, nombrePerro=nombrePerro, raza=raza, descripcion=descripcion, estado=estado)
    perro.save()
    return redirect("index")

@login_required(login_url='login')
def buscarPerro(request,id):
    perro = Perro.objects.get(pk=id)
    return HttpResponse("foto : " +perro.foto+" nombre : " +perro.nombrePerro+" raza : "+perro.raza+" descripcion : "+perro.descripcion+" estado : "+perro.estado)

@login_required(login_url='login')
def editarPerro(request,id):
    perro = Perro.objects.get(pk=id)
    return render(request,'editarPerro.html',{'perro': perro})

@login_required(login_url='login')
def editadoPerro(request,id):
    perro = Perro.objects.get(pk=id)
    foto = request.FILES.get('foto',False)
    nombrePerro = request.POST.get('nombrePerro','')
    raza = request.POST.get('raza','')
    descripcion = request.POST.get('descripcion','')
    estado = request.POST.get('estado','')
    perro.foto = foto
    perro.nombrePerro = nombrePerro
    perro.raza = raza
    perro.descripcion = descripcion
    perro.estado = estado
    perro.save()
    return redirect('index')

@login_required(login_url='login')
def eliminarPerro(request,id):
    perro = Perro.objects.get(pk=id)
    perro.delete()
    return HttpResponse("Perro Eliminado")